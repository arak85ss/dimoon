package phonebook;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Contact implements Serializable {

    private String surname;
    private String name;
    private String phoneEmail;
    private UUID id;
    private ContactType type;
}



