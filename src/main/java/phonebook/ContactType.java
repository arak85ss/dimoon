package phonebook;

public enum ContactType {

    PHONE,
    EMAIL;
}