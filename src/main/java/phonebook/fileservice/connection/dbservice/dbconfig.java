package phonebook.fileservice.connection.dbservice;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class dbconfig {

    private String url = "jdbc:postgresql://localhost:49160/phonebook";
    private String user = "postgres";
    private String password = "postgrespw";
}
