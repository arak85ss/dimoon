package phonebook.fileservice.connection.dbservice;

import phonebook.Contact;
import phonebook.iphoneBook;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

public class dbQuery implements iphoneBook  {

    private dbconnection connection = new dbconnection();
    private Contact contact;

    private UUID uuid;
    private String name;
    private String surname;
    private String phoneEmail;
    private ArrayList <String> searhResultUuidContact = new ArrayList<>();


    @Override
    public boolean addContact(UUID uuid , String name, String surname, String phoneEmail) throws SQLException {
        this.uuid = uuid;
        this.name = name;
        this.surname = surname;
        this.phoneEmail = phoneEmail;


        String regexEmail = ".*@.*";
        String regexPhone = ".*([0-9])";
        boolean emailFound = Pattern.matches(regexEmail, phoneEmail);
        boolean phoneFound = Pattern.matches(regexPhone, phoneEmail);
        String groupContact = null;

        if (emailFound) groupContact = "email";
        else if (phoneFound) groupContact = "phone";

                    try {
                        String queryAddContact = "INSERT INTO user_contact (uuid, name, surname, contact, group_contact) VALUES (?,?,?,?,?)";

                        PreparedStatement ps = connection.preparedStatementConnection(queryAddContact);
                        ps.setString(1, String.valueOf(uuid));
                        ps.setString(2, name);
                        ps.setString(3, surname);
                        ps.setString(4, phoneEmail);
                        ps.setString(5, groupContact);
                        int rows = ps.executeUpdate();

                        System.out.println("Контакт типа \\" + groupContact + "\\ добавлен \n");
                    }
                    catch(Exception ex){
                        System.out.println("Connection failed...");
                        System.out.println(ex);
                    }
        return true;
    }


    @Override
    public void AllEmail() {
        String query = "SELECT uuid, name, surname, contact, group_contact FROM user_contact WHERE group_contact LIKE 'email%'";
        Statement statement = connection.StatementConnection(query);

        try {
            ResultSet rs = statement.executeQuery(query);

            while(rs.next()) {
                String emailVal = rs.getString("contact");
                System.out.println(emailVal);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void AllPhone() {

        String query = "SELECT uuid, name, surname, contact, group_contact FROM user_contact WHERE group_contact LIKE 'phone%'";

        Statement statement = connection.StatementConnection(query);
        try {
            ResultSet rs = statement.executeQuery(query);

            while(rs.next()) {
                String phoneVal = rs.getString("contact");
                System.out.println(phoneVal);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void searchName(String SearhQuery) {

        String query = "SELECT name, surname, contact FROM user_contact WHERE name LIKE '" +SearhQuery+ "%'";
        Statement statement1 = connection.StatementConnection(query);

        try {

            ResultSet rs = statement1.executeQuery(query);
            while(rs.next()) {
                String nameVal = rs.getString("name");
                String surnameVal = rs.getString("surname");
                String contactVal = rs.getString("contact");
                System.out.print(" " + nameVal);
                System.out.print(" " + surnameVal);
                System.out.print(" " + contactVal + "\n");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searhPhoneEmail(String SearhQuery) {

     String queryEmailTable = "SELECT uuid, name, surname, contact FROM user_contact WHERE contact LIKE '"+SearhQuery+"%'";
     Statement statement1 = connection.StatementConnection(queryEmailTable);


        try {
            ResultSet rs = statement1.executeQuery(queryEmailTable);
            int count = 0;
            if(rs == null) System.out.println("Контакт не найден");

            while (rs.next()) {
                String nameVal = rs.getString("name");
                String surnameVal = rs.getString("surname");
                String contactVal = rs.getString("contact");
                String uuidVal = rs.getString("uuid");

                System.out.print("--№ " + (count++) + " " + contactVal);
                System.out.print(" " + nameVal);
                System.out.print(" " + surnameVal  + "\n");
                searhResultUuidContact.add(uuidVal);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void sortSurname() {

        String query11 = "SELECT uuid, name, surname, contact FROM user_contact ORDER BY surname ASC";
        Statement st11 = connection.StatementConnection(query11);

        try {
            ResultSet rs11 = st11.executeQuery(query11);
            while(rs11.next()) {
                String surnameVal = rs11.getString("surname");
                String nameVal = rs11.getString("name");
                String uuidVal = rs11.getString("uuid");
                String contact = rs11.getString("contact");
                System.out.print(" " + surnameVal);
                System.out.print(" " + nameVal);
                System.out.print(" " + contact + "\n");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void deleteContact(int NumberDeleteContact) {

        if(NumberDeleteContact < 0 || NumberDeleteContact > searhResultUuidContact.size()) {
            System.out.println("Неверное значение!");
            return;
        }

        // получаем id контакта
        String contactUuidValye = searhResultUuidContact.get(NumberDeleteContact);

        // выбор таблички для удаления phone/email
        //System.out.println(phoneEmailTable);


            //String query = "DELETE FROM "+phoneEmailTable+" WHERE uuid ='" + contactUuidValye + "'";

            String query = "DELETE FROM user_contact WHERE uuid ='"+contactUuidValye+"'";
            Statement statementDeleteContact = connection.StatementConnection(query);
        System.out.println("Контакт удален!");
            try {
                statementDeleteContact.executeQuery(query);

            } catch (SQLException e) {
                //e.printStackTrace();
            }
        }
    }
