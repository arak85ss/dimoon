package phonebook.fileservice.connection.dbservice;

import java.rmi.server.UID;
import java.sql.*;

public class dbconnection {

    private dbconfig dbconf = new dbconfig();
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;



    public Statement StatementConnection(String query) {

        try {
            connection = DriverManager.getConnection(dbconf.getUrl(), dbconf.getUser(), dbconf.getPassword());
            statement = connection.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }



    public PreparedStatement preparedStatementConnection(String query) {

        try {
            connection = DriverManager.getConnection(dbconf.getUrl(), dbconf.getUser(), dbconf.getPassword());
            statement = connection.createStatement();
            preparedStatement = connection.prepareStatement(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return preparedStatement;
    }
}