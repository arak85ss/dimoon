INSERT INTO user_contact (uuid, name, surname, contact, group_contact) VALUES
('Marina', 'Pupkina', 'marina@gmail.com', '49804cdf-9028-42b2-bdd7-ea07fc797777', 'email'),
('Alina', 'Matweeva', 'alina@gmail.com', '49804cdf-9028-42b2-bdd7-ea07fc7555555', 'email'),
('Alla', 'Chehova', 'chehova@gmail.com', '49804cdf-9028-42b2-bdd7-ea07fc7934343', 'email'),
('Pavel', 'Antipov', 'pavelantipov@gmail.com', '49804cdf-9028-42b2-bdd7-ea07fc796000', 'phone'),
('Samson', 'Flipow', '0957331245', '49804cdf-9028-42b2-bdd7-ea07fc793434', 'phone'),
('Galina', 'Milka', '0958568989', '49804cdf-9028-42b2-bdd7-ea07fc796122', 'phone'),
('Alexandr', 'Zlotov', '0668954578', '49804cdf-9028-42b2-bdd7-ea07fc796888', 'phone'),
('Max', 'Ivanov', '0964567888', '49804cdf-9028-42b2-bdd7-49804cdf', 'phone');