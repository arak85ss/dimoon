CREATE TABLE user_contact (
    uuid VARCHAR(100) NOT NULL UNIQUE PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    contact VARCHAR(150) NOT NULL,
    group_contact VARCHAR(50) NOT NULL
);